# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.5.0](http://bitbucket.org/lividturkey/lerna-example/compare/v0.4.1...v0.5.0) (2020-12-03)

**Note:** Version bump only for package @jimmydc/monorepo-test1





## [0.4.1](http://bitbucket.org/lividturkey/lerna-example/compare/v0.4.0...v0.4.1) (2020-12-03)

**Note:** Version bump only for package @jimmydc/monorepo-test1





# 0.4.0 (2020-12-03)


### Bug Fixes

* add readme to monorepo-test1 ([6264b3f](http://bitbucket.org/lividturkey/lerna-example/commits/6264b3f75a0bda1f4c3f74688a8fd2bbe2a8a6d4))





# [0.3.0](https://github.com/Jimmydalecleveland/lerna-example/compare/v0.2.3...v0.3.0) (2020-12-03)

**Note:** Version bump only for package @jimmydc/monorepo-test1





## [0.2.3](https://github.com/Jimmydalecleveland/lerna-example/compare/v0.2.2...v0.2.3) (2020-12-03)


### Bug Fixes

* add readme to monorepo-test1 ([6264b3f](https://github.com/Jimmydalecleveland/lerna-example/commit/6264b3f75a0bda1f4c3f74688a8fd2bbe2a8a6d4))

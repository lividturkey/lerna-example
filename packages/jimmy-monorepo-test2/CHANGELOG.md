# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.5.0](http://bitbucket.org/lividturkey/lerna-example/compare/v0.4.1...v0.5.0) (2020-12-03)


### Features

* added a feat to package 2 ([ee17cef](http://bitbucket.org/lividturkey/lerna-example/commits/ee17cef0b0ba8df2204f6472e32e3f857605a5bc))





## [0.4.1](http://bitbucket.org/lividturkey/lerna-example/compare/v0.4.0...v0.4.1) (2020-12-03)

**Note:** Version bump only for package @jimmydc/monorepo-test2





# 0.4.0 (2020-12-03)


### Features

* add readme to monorepo-test2, lerna publish ([2a049fe](http://bitbucket.org/lividturkey/lerna-example/commits/2a049fe4855d07338d4a8b88816b8fce25b1d291))





# [0.3.0](https://github.com/Jimmydalecleveland/lerna-example/compare/v0.2.3...v0.3.0) (2020-12-03)


### Features

* add readme to monorepo-test2, lerna publish ([2a049fe](https://github.com/Jimmydalecleveland/lerna-example/commit/2a049fe4855d07338d4a8b88816b8fce25b1d291))





## [0.2.3](https://github.com/Jimmydalecleveland/lerna-example/compare/v0.2.2...v0.2.3) (2020-12-03)

**Note:** Version bump only for package @jimmydc/monorepo-test2
